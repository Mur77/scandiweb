import { iProductCard } from '../../interfaces'

export const productData: iProductCard[] = [
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product1.png',
        stock: true,
    },
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product2.png',
        stock: true,
    },
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product3.png',
        stock: false,
    },
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product4.png',
        stock: true,
    },
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product1.png',
        stock: true,
    },
    {
        title: 'Apollo Running Short',
        price: 50,
        imageLink: '/images/product2.png',
        stock: true,
    },
]