import React from 'react'

import { ProductCard } from './productCard/ProductCard'

import { productData } from './productData'

import styles from './Category.module.scss'

export class Category extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <div className={styles.innerContainer}>
                    {productData.map((item, index) => {
                        return (
                            <ProductCard key={index} { ...item } />
                        )
                    })}
                </div>
            </div>
        )
    }
}
