import React from 'react'

import { iProductCard } from '../../../interfaces'
import { ReactComponent as GreenCart } from '../../../icons/green-cart.svg'

import styles from './ProductCard.module.scss'

interface iProductCardState {
    selected: boolean
}

export class ProductCard extends React.Component<iProductCard, iProductCardState> {
    constructor(props: Readonly<iProductCard>) {
        super(props)
        this.state = { selected: false }
    }

    handleMouseEnter(e: React.MouseEvent) {
        if (e.type === 'mouseenter') {
            this.setState({ selected: true })
        } else if (e.type === 'mouseleave') {
            this.setState({ selected: false })
        }
    }

    render() {
        return (
            <div
                className={`${styles.container} 
                    ${this.state.selected && this.props.stock ? styles.selected : ''} 
                    ${!this.props.stock ? styles.dimmed : ''}`}
                onMouseEnter={(e)=>this.handleMouseEnter(e)}
                onMouseLeave={(e)=>this.handleMouseEnter(e)}
            >
                <div className={styles.imageContainer}>
                    <img className={styles.image} src={this.props.imageLink} alt={this.props.title} />
                    {!this.props.stock && <span className={styles.outOfStock}>OUT OF STOCK</span>}
                </div>
                <div className={styles.title}>{this.props.title}</div>
                <div className={styles.price}>{this.props.price}</div>
                {this.state.selected && this.props.stock &&
                    <div className={styles.cart}>
                        <GreenCart />
                    </div>
                }
            </div>
        )
    }
}
