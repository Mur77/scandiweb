export interface iProductCard {
    title: string
    price: number
    imageLink: string
    stock: boolean
}
