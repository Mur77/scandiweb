import React from 'react'

import { ReactComponent as LogoSvg } from '../../../icons/logo.svg'

import styles from './Logo.module.scss'

export class Logo extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <LogoSvg />
            </div>
        )
    }
}
