import React from 'react'

import { Sex } from './sex/Sex'
import { Logo } from './logo/Logo'
import { CurrencySelector } from './currencySelector/CurrencySelector'
import { CartIcon } from './cartIcon/CartIcon'

import styles from './Header.module.scss'

export class Header extends React.Component {

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.innerContainer}>
                    <Sex />
                    <Logo />
                    <div className={styles.rightSide}>
                        <CurrencySelector />
                        <CartIcon />
                    </div>
                </div>
            </div>
        )
    }
}
