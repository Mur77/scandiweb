import React from 'react'

import { ReactComponent as Cart } from '../../../icons/cart.svg'

import styles from './CartIcon.module.scss'

export class CartIcon extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <Cart />
            </div>
        )
    }
}
