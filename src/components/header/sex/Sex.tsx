import React from 'react'

import styles from './Sex.module.scss'

export class Sex extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <div className={styles.item}>WOMEN</div>
                <div className={styles.item}>MEN</div>
                <div className={styles.item}>KIDS</div>
            </div>
        )
    }
}