import React from 'react'

import { ReactComponent as ArrowUp } from '../../../icons/arrow-up.svg'
import { ReactComponent as ArrowDown } from '../../../icons/arrow-down.svg'

import styles from './CurrencySelector.module.scss'

interface iCurrencySelectorProps {}

interface iCurrencySelectorState {
    selectorOpen: boolean
}

export class CurrencySelector extends React.Component<iCurrencySelectorProps, iCurrencySelectorState> {
    constructor(props: iCurrencySelectorProps) {
        super(props)

        this.state = {
            selectorOpen: false,
        }
    }

    render() {
        return (
            <div className={styles.container}>
                <span>$</span>
                {this.state.selectorOpen ? <ArrowUp /> : <ArrowDown />}
            </div>
        )
    }
}
