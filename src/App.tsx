import React from 'react'

import { Header } from './components/header/Header'
import { Category } from './views/category/Category'

import './App.css';

function App() {
    return (
        <div className="App">
            <Header />
            <Category />
        </div>
    );
}

export default App;
